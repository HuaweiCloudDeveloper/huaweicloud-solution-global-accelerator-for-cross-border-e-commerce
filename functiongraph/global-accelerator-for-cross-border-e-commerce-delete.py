import time
import traceback
from huaweicloudsdkcore.auth.credentials import BasicCredentials
from huaweicloudsdkcore.exceptions import exceptions
from huaweicloudsdkvpc.v2.region.vpc_region import VpcRegion
import huaweicloudsdkvpc.v2 as vpc
from huaweicloudsdkecs.v2.region.ecs_region import EcsRegion
import huaweicloudsdkecs.v2 as ecs
from huaweicloudsdkelb.v3.region.elb_region import ElbRegion
import huaweicloudsdkelb.v3 as elb
from huaweicloudsdkeip.v2.region.eip_region import EipRegion
import huaweicloudsdkeip.v2 as eip
from huaweicloudsdkga.v1.region.ga_region import GaRegion
import huaweicloudsdkga.v1 as ga


def handler(event, context):
    ak = context.getAccessKey()
    sk = context.getSecretKey()
    log = context.getLogger()
    delete = Delete(context)
    delete.start_process()


class Delete:

    def __init__(self, context):
        self.region_ids = eval(context.getUserData("region_ids"))
        self.ga_client = self.get_ga_client(context)
        self.acclerator_id = []
        self.acclerator_id_delete = []
        self.listener_ids = []
        self.listener_ids_delete = []
        self.endpoint_group = []
        self.endpoint_group_delete = []
        self.endpoint_id = []
        self.endpoint_delete = {}
        self.healthcheck = []
        self.healthcheck_delete = []

        self.list_elb_client = []
        self.get_elb_client(context)
        self.elb_ids_delete = []
        self.elb_ids = []
        self.elb_id = []

        self.list_eip_client = []
        self.get_eip_client(context)
        self.eip_id = []
        self.eip_ids = []
        self.eip_response = []

        self.list_ecs_client = []
        self.get_ecs_client(context)
        self.ecs_response = []
        self.ecs_list = []
        self.ecs_delete = []
        self.ecs_delete_ids = []

        self.list_vpc_client = []
        self.get_vpc_client(context)
        self.vpc_response = []
        self.vpc_ids = []
        self.vpc_id = []
        self.subnet_response = []
        self.subnet_ids = []
        self.subnet_id = []

    def get_ga_client(self, context):
        credentials = BasicCredentials(context.getAccessKey(), context.getSecretKey())
        client = ga.GaClient.new_builder() \
            .with_credentials(credentials) \
            .with_region(GaRegion.value_of("cn-east-3")) \
            .build()
        return client

    def get_accelerator(self):
        try:
            request = ga.ListAcceleratorsRequest()
            response = self.ga_client.list_accelerators(request)
            self.acclerator_id.append(response.to_dict())
            for index, item in enumerate(self.acclerator_id[0]["accelerators"]):
                if item['tags'] != []:
                    for i, tags in enumerate(item['tags']):
                        if tags['key'] == 'sac':
                            self.acclerator_id_delete.append(self.acclerator_id[0]['accelerators'][index]['id'])
        except exceptions.ClientRequestException as e:
            self.log.error(
                f"status_code:{e.status_code}, "
                f"request_id:{e.request_id}, "
                f"error_code:{e.error_code}. "
                f"error_msg:{e.error_msg}")

    def get_listener(self):
        try:
            request = ga.ListListenersRequest()
            response = self.ga_client.list_listeners(request)
            self.listener_ids.append(response.to_dict())
            for index, item in enumerate(self.listener_ids[0]["listeners"]):
                for i, tags in enumerate(item['tags']):
                    if tags['key'] == 'sac':
                        self.listener_ids_delete.append(self.listener_ids[0]['listeners'][index]['id'])
        except exceptions.ClientRequestException as e:
            self.log.error(
                f"status_code:{e.status_code}, "
                f"request_id:{e.request_id}, "
                f"error_code:{e.error_code}. "
                f"error_msg:{e.error_msg}")

    def get_endpoint_group(self):
        try:
            request = ga.ListEndpointGroupsRequest()
            response = self.ga_client.list_endpoint_groups(request)
            self.endpoint_group.append(response.to_dict())
            for index, item in enumerate(self.endpoint_group[0]["endpoint_groups"]):
                for i, ids in enumerate(item['listeners']):
                    if ids['id'] in self.listener_ids_delete:
                        self.endpoint_group_delete.append(self.endpoint_group[0]['endpoint_groups'][index]['id'])
        except exceptions.ClientRequestException as e:
            self.log.error(
                f"status_code:{e.status_code}, "
                f"request_id:{e.request_id}, "
                f"error_code:{e.error_code}. "
                f"error_msg:{e.error_msg}")

    def get_healthcheck(self):
        for item in self.endpoint_group_delete:
            try:
                request = ga.ListHealthChecksRequest()
                request.endpoint_group_id = item
                response = self.ga_client.list_health_checks(request)
                self.healthcheck.append(response.to_dict())
                if self.healthcheck[0]['health_checks'] != []:
                    self.healthcheck_delete.append(self.healthcheck[0]['health_checks'][0]['id'])
                self.healthcheck = []
            except exceptions.ClientRequestException as e:
                self.log.error(
                               f"status_code:{e.status_code}, "
                               f"request_id:{e.request_id}, "
                               f"error_code:{e.error_code}. "
                               f"error_msg:{e.error_msg}")

    def get_endpoint(self):
        time.sleep(10)
        for index, item in enumerate(self.endpoint_group_delete):
            try:
                request = ga.ListEndpointsRequest()
                request.endpoint_group_id = item
                response = self.ga_client.list_endpoints(request)
                self.endpoint_id.append(response.to_dict())
                self.endpoint_delete[item] = []
                for i in self.endpoint_id:
                    for j in i['endpoints']:
                        self.endpoint_delete[item].append(j['id'])
                self.endpoint_id = []
            except exceptions.ClientRequestException as e:
                self.log.error(
                               f"status_code:{e.status_code}, "
                               f"request_id:{e.request_id}, "
                               f"error_code:{e.error_code}. "
                               f"error_msg:{e.error_msg}")

    def delete_endpoint(self):
        time.sleep(20)
        for key in self.endpoint_delete.keys():
            for j in self.endpoint_delete[key]:
                try:
                    request = ga.DeleteEndpointRequest()
                    request.endpoint_group_id = key
                    request.endpoint_id = j
                    response = self.ga_client.delete_endpoint(request)
                except exceptions.ClientRequestException as e:
                    self.log.error(
                        f"status_code:{e.status_code}, "
                        f"request_id:{e.request_id}, "
                        f"error_code:{e.error_code}. "
                        f"error_msg:{e.error_msg}")

    def delete_healthcheck(self):
        time.sleep(10)
        for item in self.healthcheck_delete:
            try:
                request = ga.DeleteHealthCheckRequest()
                request.health_check_id = item
            except exceptions.ClientRequestException as e:
                self.log.error(
                               f"status_code:{e.status_code}, "
                               f"request_id:{e.request_id}, "
                               f"error_code:{e.error_code}. "
                               f"error_msg:{e.error_msg}")

    def delete_endpoint_group(self):
        time.sleep(30)
        for item in self.endpoint_group_delete:
            try:
                request = ga.DeleteEndpointGroupRequest()
                request.endpoint_group_id = item
                response = self.ga_client.delete_endpoint_group(request)
            except exceptions.ClientRequestException as e:
                self.log.error(
                               f"status_code:{e.status_code}, "
                               f"request_id:{e.request_id}, "
                               f"error_code:{e.error_code}. "
                               f"error_msg:{e.error_msg}")

    def delete_listeners(self):
        time.sleep(20)
        for item in self.listener_ids_delete:
            try:
                request = ga.DeleteListenerRequest()
                request.listener_id = item
                response = self.ga_client.delete_listener(request)
            except exceptions.ClientRequestException as e:
                self.log.error(
                               f"status_code:{e.status_code}, "
                               f"request_id:{e.request_id}, "
                               f"error_code:{e.error_code}. "
                               f"error_msg:{e.error_msg}")

    def delete_accelerator(self):
        time.sleep(20)
        for item in self.acclerator_id_delete:
            try:
                request = ga.DeleteAcceleratorRequest()
                request.accelerator_id = item
                response = self.ga_client.delete_accelerator(request)
            except exceptions.ClientRequestException as e:
                self.log.error(
                               f"status_code:{e.status_code}, "
                               f"request_id:{e.request_id}, "
                               f"error_code:{e.error_code}. "
                               f"error_msg:{e.error_msg}")

    def get_elb_client(self, context):
        for region in self.region_ids:
            credentials = BasicCredentials(context.getAccessKey(), context.getSecretKey())
            client = elb.ElbClient.new_builder() \
                .with_credentials(credentials) \
                .with_region(ElbRegion.value_of(region)) \
                .build()
            self.list_elb_client.append(client)

    def list_elb(self):
        for index, client in enumerate(self.list_elb_client):
            try:
                request = elb.ListLoadBalancersRequest()
                response = client.list_load_balancers(request)
                self.elb_id.append(response.to_dict())
                for index, item in enumerate(self.elb_id[0]['loadbalancers']):
                    if item['tags'] != []:
                        for i, tags in enumerate(item['tags']):
                            if tags['key'] == 'sac':
                                self.elb_ids.append(self.elb_id[0]['loadbalancers'][index]['id'])
                self.elb_ids_delete.append(self.elb_ids)
                self.elb_ids = []
                self.elb_id = []
            except exceptions.ClientRequestException as e:
                self.log.error(
                               f"status_code:{e.status_code}, "
                               f"request_id:{e.request_id}, "
                               f"error_code:{e.error_code}. "
                               f"error_msg:{e.error_msg}")
    def delete_elb(self):
        time.sleep(10)
        for index, client in enumerate(self.list_elb_client):
            for item in self.elb_ids_delete[index]:
                try:
                    request = elb.DeleteLoadBalancerForceRequest()
                    request.loadbalancer_id = item
                    response = client.delete_load_balancer_force(request)
                except exceptions.ClientRequestException as e:
                    self.log.error(
                        f"status_code:{e.status_code}, "
                        f"request_id:{e.request_id}, "
                        f"error_code:{e.error_code}. "
                        f"error_msg:{e.error_msg}")

    def get_eip_client(self, context):
        for region in self.region_ids:
            credentials = BasicCredentials(context.getAccessKey(), context.getSecretKey())
            client = eip.EipClient.new_builder() \
                .with_credentials(credentials) \
                .with_region(EipRegion.value_of(region)) \
                .build()
            self.list_eip_client.append(client)

    def list_eip(self):
        for index, client in enumerate(self.list_eip_client):
            try:
                request = eip.ListPublicipsByTagsRequest()
                listValuesTags = [
                    "key"
                ]
                listTagsbody = [
                    eip.TagReq(
                        key="sac",
                        values=listValuesTags
                    )
                ]
                request.body = eip.ListPublicipsByTagsRequestBody(
                    action="filter",
                    tags=listTagsbody
                )
                response = client.list_publicips_by_tags(request)
                self.eip_response.append(response.to_dict())
                for item in self.eip_response[0]['resources']:
                    self.eip_id.append(item['resource_id'])
                self.eip_ids.append(self.eip_id)
                self.eip_response = []
                self.eip_id = []
            except exceptions.ClientRequestException as e:
                self.log.error(
                               f"status_code:{e.status_code}, "
                               f"request_id:{e.request_id}, "
                               f"error_code:{e.error_code}. "
                               f"error_msg:{e.error_msg}")

    def delete_eip(self):
        for index, client in enumerate(self.list_eip_client):
            for item in self.eip_ids[index]:
                try:
                    request = eip.DeletePublicipRequest()
                    request.publicip_id = item
                    response = client.delete_publicip(request)
                except exceptions.ClientRequestException as e:
                    self.log.error(
                        f"status_code:{e.status_code}, "
                        f"request_id:{e.request_id}, "
                        f"error_code:{e.error_code}. "
                        f"error_msg:{e.error_msg}")

    def get_ecs_client(self, context):
        for region in self.region_ids:
            credentials = BasicCredentials(context.getAccessKey(), context.getSecretKey())
            client = ecs.EcsClient.new_builder() \
                .with_credentials(credentials) \
                .with_region(EcsRegion.value_of(region)) \
                .build()
            self.list_ecs_client.append(client)

    def list_ecs(self):
        for index, client in enumerate(self.list_ecs_client):
            try:
                request = ecs.ListServersDetailsRequest()
                request.tags = "sac=key"
                response = client.list_servers_details(request)
                self.ecs_response.append(response.to_dict())
                self.ecs_list.append(self.ecs_response[0]['servers'])
                for i, item in enumerate(self.ecs_list[0]):
                    self.ecs_delete.append(item['id'])
                self.ecs_delete_ids.append(self.ecs_delete)
                self.ecs_delete = []
                self.ecs_list = []
                self.ecs_response = []
            except exceptions.ClientRequestException as e:
                self.log.error(
                               f"status_code:{e.status_code}, "
                               f"request_id:{e.request_id}, "
                               f"error_code:{e.error_code}. "
                               f"error_msg:{e.error_msg}")

    def delete_ecs(self):
        for index, client in enumerate(self.list_ecs_client):
            for item in self.ecs_delete_ids[index]:
                try:
                    request = ecs.DeleteServersRequest()
                    listServersbody = [
                        ecs.ServerId(
                            id=item
                        )
                    ]
                    request.body = ecs.DeleteServersRequestBody(
                        servers=listServersbody
                    )
                    response = client.delete_servers(request)
                except exceptions.ClientRequestException as e:
                    self.log.error(
                        f"status_code:{e.status_code}, "
                        f"request_id:{e.request_id}, "
                        f"error_code:{e.error_code}. "
                        f"error_msg:{e.error_msg}")

    def get_vpc_client(self, context):
        for region in self.region_ids:
            credentials = BasicCredentials(context.getAccessKey(), context.getSecretKey())
            client = vpc.VpcClient.new_builder() \
                .with_credentials(credentials) \
                .with_region(VpcRegion.value_of(region)) \
                .build()
            self.list_vpc_client.append(client)

    def list_vpc(self):
        for index, client in enumerate(self.list_vpc_client):
            try:
                request = vpc.ListVpcsByTagsRequest()
                listValuesTags = [
                    "key"
                ]
                listTagsbody = [
                    vpc.ListTag(
                        key="sac",
                        values=listValuesTags
                    )
                ]
                request.body = vpc.ListVpcsByTagsRequestBody(
                    tags=listTagsbody,
                    action="filter"
                )
                response = client.list_vpcs_by_tags(request)
                self.vpc_response.append(response.to_dict())
                if self.vpc_response[0]['resources'] != []:
                    self.vpc_id.append(self.vpc_response[0]['resources'][0]['resource_id'])
                self.vpc_ids.append(self.vpc_id)
                self.vpc_response = []
                self.vpc_id = []
            except exceptions.ClientRequestException as e:
                self.log.error(
                               f"status_code:{e.status_code}, "
                               f"request_id:{e.request_id}, "
                               f"error_code:{e.error_code}. "
                               f"error_msg:{e.error_msg}")

    def list_subnet(self):
        for index, client in enumerate(self.list_vpc_client):
            try:
                request = vpc.ListSubnetsByTagsRequest()
                listValuesTags = [
                    "key"
                ]
                listTagsbody = [
                    vpc.ListTag(
                        key="sac",
                        values=listValuesTags
                    )
                ]
                request.body = vpc.ListSubnetsByTagsRequestBody(
                    tags=listTagsbody,
                    action="filter"
                )
                response = client.list_subnets_by_tags(request)
                self.subnet_response.append(response.to_dict())
                if self.subnet_response[0]['resources'] != []:
                    self.subnet_id.append(self.subnet_response[0]['resources'][0]['resource_id'])
                self.subnet_ids.append(self.subnet_id)
                self.subnet_response = []
                self.subnet_id = []
            except exceptions.ClientRequestException as e:
                self.log.error(
                               f"status_code:{e.status_code}, "
                               f"request_id:{e.request_id}, "
                               f"error_code:{e.error_code}. "
                               f"error_msg:{e.error_msg}")

    def delete_subnet(self):
        for index, client in enumerate(self.list_vpc_client):
            for i, item in enumerate(self.subnet_ids[index]):
                try:
                    request = vpc.DeleteSubnetRequest()
                    request.vpc_id = self.vpc_ids[index][i]
                    request.subnet_id = item
                    response = client.delete_subnet(request)
                except exceptions.ClientRequestException as e:
                    self.log.error(
                        f"status_code:{e.status_code}, "
                        f"request_id:{e.request_id}, "
                        f"error_code:{e.error_code}. "
                        f"error_msg:{e.error_msg}")

    def delete_vpc(self):
        time.sleep(10)
        for index, client in enumerate(self.list_vpc_client):
            for item in self.vpc_ids[index]:
                try:
                    request = vpc.DeleteVpcRequest()
                    request.vpc_id = item
                    response = client.delete_vpc(request)
                except exceptions.ClientRequestException as e:
                    self.log.error(
                        f"status_code:{e.status_code}, "
                        f"request_id:{e.request_id}, "
                        f"error_code:{e.error_code}. "
                        f"error_msg:{e.error_msg}")

    def start_process(self):
        self.list_elb()
        self.delete_elb()
        self.list_ecs()
        self.delete_ecs()
        self.get_accelerator()
        self.get_listener()
        self.get_endpoint_group()
        self.get_endpoint()
        self.get_healthcheck()
        self.delete_endpoint()
        self.delete_healthcheck()
        self.delete_endpoint_group()
        self.delete_listeners()
        self.delete_accelerator()
        self.list_eip()
        self.delete_eip()
        self.list_subnet()
        self.list_vpc()
        self.delete_subnet()
        self.delete_vpc()