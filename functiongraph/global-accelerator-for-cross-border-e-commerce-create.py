# -*- coding:utf-8 -*-
import time
import traceback
from huaweicloudsdkcore.auth.credentials import BasicCredentials
from huaweicloudsdkcore.exceptions import exceptions
from huaweicloudsdkvpc.v2.region.vpc_region import VpcRegion
import huaweicloudsdkvpc.v2 as vpc
from huaweicloudsdkecs.v2.region.ecs_region import EcsRegion
import huaweicloudsdkecs.v2 as ecs
from huaweicloudsdkelb.v3.region.elb_region import ElbRegion
import huaweicloudsdkelb.v3 as elb
from huaweicloudsdkeip.v2.region.eip_region import EipRegion
import huaweicloudsdkeip.v2 as eip
from huaweicloudsdkga.v1.region.ga_region import GaRegion
import huaweicloudsdkga.v1 as ga


def handler(event, context):
    ak = context.getAccessKey()
    sk = context.getSecretKey()
    log = context.getLogger()

    create = Create(context)
    create.start_process()


class Create:

    def __init__(self, context):
        self.region_ids = eval(context.getUserData("region_ids"))

        self.list_vpc_client = []
        self.get_vpc_client(context)
        self.vpc_response = []
        self.vpc_ids = []
        self.subnet_response = []
        self.subnet_ids = []
        self.ipv4_subnet_ids = []

        self.list_ecs_client = []
        self.get_ecs_client(context)
        self.ecs_count = eval(context.getUserData("ecs_count"))
        self.ecs_flavor_ref = eval(context.getUserData("ecs_flavor_ref"))
        self.ecs_image_ref = eval(context.getUserData("ecs_image_ref"))
        self.ecs_volume_type =eval(context.getUserData("ecs_volume_type"))
        self.ecs_az_response = []
        self.ecs_az_ids = []
        self.ecs_azs_ids = []
        self.ecs_response = []
        self.ecs_id = []
        self.ecs_ids = []
        self.show_ecs_response = []
        self.ecs_ip = []
        self.tmp = []
        self.ecs_ips = []

        self.list_eip_client = []
        self.get_eip_client(context)
        self.eip_type = eval(context.getUserData("eip_type"))
        self.eip_response = []
        self.eip_ids = []
        self.eip_id = []
        self.eip_ips = []
        self.eip_ip = []

        self.list_elb_client = []
        self.get_elb_client(context)
        self.elb_algorithm = eval(context.getUserData("elb_algorithm"))
        self.elb_member_protocol_port = eval(context.getUserData("elb_member_protocol_port"))
        self.elb_protocol = eval(context.getUserData("elb_protocol"))
        self.elb_protocol_port = eval(context.getUserData("elb_protocol_port"))
        self.elb_response = []
        self.elb_ids = []
        self.elb_pool_response = []
        self.elb_pool_ids = []

        self.ga_client = self.get_ga_client(context)
        self.ga_area = eval(context.getUserData("ga_area"))
        self.list_accelerators = []
        self.ga_port_list = eval(context.getUserData("ga_port_list"))
        self.ga_endpoint_group = eval(context.getUserData("ga_endpoint_group"))
        self.ga_healthcheck_port = eval(context.getUserData("ga_healthcheck_port"))
        self.ga_protocol = eval(context.getUserData("ga_protocol"))
        self.list_listeners = []
        self.list_endpoint_groups = []

    def get_vpc_client(self, context):
        for region in self.region_ids:
            credentials = BasicCredentials(context.getAccessKey(), context.getSecretKey())
            client = vpc.VpcClient.new_builder() \
                .with_credentials(credentials) \
                .with_region(VpcRegion.value_of(region)) \
                .build()
            self.list_vpc_client.append(client)


    def create_vpc(self):
        for index, client in enumerate(self.list_vpc_client):
            try:
                request = vpc.CreateVpcRequest()
                vpcbody = vpc.CreateVpcOption(
                    cidr="192.168.0.0/24",
                    name='global-accelerator-for-cross-border-e-commerce'
                )
                request.body = vpc.CreateVpcRequestBody(
                    vpc=vpcbody
                )
                response = client.create_vpc(request)
                self.vpc_response.append(response.to_dict())
                self.vpc_ids.append(self.vpc_response[0]['vpc']['id'])
                self.vpc_response = []
            except exceptions.ClientRequestException as e:
                self.log.error(
                               f"status_code:{e.status_code}, "
                               f"request_id:{e.request_id}, "
                               f"error_code:{e.error_code}. "
                               f"error_msg:{e.error_msg}")

    def create_vpc_tag(self):
        for index, client in enumerate(self.list_vpc_client):
            try:
                request = vpc.CreateVpcResourceTagRequest()
                request.vpc_id = self.vpc_ids[index]
                tagbody = vpc.ResourceTag(
                    key="sac",
                    value="key"
                )
                request.body = vpc.CreateVpcResourceTagRequestBody(
                    tag=tagbody
                )
                response = client.create_vpc_resource_tag(request)
            except exceptions.ClientRequestException as e:
                self.log.error(
                               f"status_code:{e.status_code}, "
                               f"request_id:{e.request_id}, "
                               f"error_code:{e.error_code}. "
                               f"error_msg:{e.error_msg}")
    def create_subnet(self):
        for index, client in enumerate(self.list_vpc_client):
            try:
                request = vpc.CreateSubnetRequest()
                listDnsListSubnet = [
                    "100.125.1.250",
                    "100.125.129.250"
                ]
                subnetbody = vpc.CreateSubnetOption(
                    name='global-accelerator-for-cross-border-e-commerce',
                    cidr="192.168.0.0/26",
                    vpc_id=self.vpc_ids[index],
                    gateway_ip="192.168.0.1",
                    dns_list=listDnsListSubnet
                )
                request.body = vpc.CreateSubnetRequestBody(
                    subnet=subnetbody
                )
                response = client.create_subnet(request)
                self.subnet_response.append(response.to_dict())
                self.subnet_ids.append(self.subnet_response[0]['subnet']['id'])
                self.ipv4_subnet_ids.append(self.subnet_response[0]['subnet']['neutron_subnet_id'])
                self.subnet_response = []
            except exceptions.ClientRequestException as e:
                self.log.error(
                               f"status_code:{e.status_code}, "
                               f"request_id:{e.request_id}, "
                               f"error_code:{e.error_code}. "
                               f"error_msg:{e.error_msg}")


    def create_subnet_tag(self):
        for index, client in enumerate(self.list_vpc_client):
            try:
                request = vpc.CreateSubnetTagRequest()
                request.subnet_id = self.subnet_ids[index]
                tagbody = vpc.ResourceTag(
                    key="sac",
                    value="key"
                )
                request.body = vpc.CreateSubnetTagRequestBody(
                    tag=tagbody
                )
                response = client.create_subnet_tag(request)
            except exceptions.ClientRequestException as e:
                self.log.error(
                               f"status_code:{e.status_code}, "
                               f"request_id:{e.request_id}, "
                               f"error_code:{e.error_code}. "
                               f"error_msg:{e.error_msg}")

    def get_ecs_client(self, context):
        for region in self.region_ids:
            credentials = BasicCredentials(context.getAccessKey(), context.getSecretKey())
            client = ecs.EcsClient.new_builder() \
                .with_credentials(credentials) \
                .with_region(EcsRegion.value_of(region)) \
                .build()
            self.list_ecs_client.append(client)

    def list_ecs_az(self):
        for index, client in enumerate(self.list_ecs_client):
            try:
                request = ecs.NovaListAvailabilityZonesRequest()
                response = client.nova_list_availability_zones(request)
                self.ecs_az_response.append(response.to_dict())
                self.ecs_az_ids.append(self.ecs_az_response[0]['availability_zone_info'][0]['zone_name'])
                self.ecs_az_ids.append(self.ecs_az_response[0]['availability_zone_info'][1]['zone_name'])
                self.ecs_azs_ids.append(self.ecs_az_ids)
                self.ecs_az_response = []
                self.ecs_az_ids = []
            except exceptions.ClientRequestException as e:
                self.log.error(
                               f"status_code:{e.status_code}, "
                               f"request_id:{e.request_id}, "
                               f"error_code:{e.error_code}. "
                               f"error_msg:{e.error_msg}")

    def create_ecs(self):
        time.sleep(5)
        for index, client in enumerate(self.list_ecs_client):
            for i, item in enumerate(self.ecs_azs_ids[index]):
                try:
                    request = ecs.CreatePostPaidServersRequest()
                    listServerTagsServer = [
                        ecs.PostPaidServerTag(
                            key="sac",
                            value="key"
                        )
                    ]
                    rootVolumeServer = ecs.PostPaidServerRootVolume(
                        volumetype=self.ecs_volume_type,
                        size=400
                    )
                    listNicsServer = [
                        ecs.PostPaidServerNic(
                            subnet_id=self.subnet_ids[index]
                        )
                    ]
                    listMetadataServer = {
                        "__support_agent_list": "hss,hss-ent"
                    }
                    extendparamServer = ecs.PostPaidServerExtendParam(
                        region_id=self.region_ids[index]
                    )
                    serverbody = ecs.PostPaidServer(
                        availability_zone=item,
                        count=self.ecs_count[index][i],
                        extendparam=extendparamServer,
                        flavor_ref=self.ecs_flavor_ref,
                        image_ref=self.ecs_image_ref[index],
                        metadata=listMetadataServer,
                        name='global-accelerator-for-cross-border-e-commerce',
                        nics=listNicsServer,
                        root_volume=rootVolumeServer,
                        server_tags=listServerTagsServer,
                        vpcid=self.vpc_ids[index]
                    )
                    request.body = ecs.CreatePostPaidServersRequestBody(
                        server=serverbody
                    )
                    response = client.create_post_paid_servers(request)
                    self.ecs_response.append(response.to_dict())
                    self.ecs_id.extend(self.ecs_response[0]['server_ids'])
                    self.ecs_response = []
                except exceptions.ClientRequestException as e:
                    self.log.error(
                        f"status_code:{e.status_code}, "
                        f"request_id:{e.request_id}, "
                        f"error_code:{e.error_code}. "
                        f"error_msg:{e.error_msg}")
            self.ecs_ids.append(self.ecs_id)
            self.ecs_id = []


    def show_ecs(self):
        time.sleep(20)
        for index, client in enumerate(self.list_ecs_client):
            for i, ids in enumerate(self.ecs_ids[index]):
                try:
                    request = ecs.ShowServerRequest()
                    request.server_id = ids
                    response = client.show_server(request)
                    self.show_ecs_response.append(response.to_dict())
                    self.tmp = list(self.show_ecs_response[0]['server']['addresses'].values())
                    self.ecs_ip.append(self.tmp[0][0].to_dict()['addr'])
                    self.show_ecs_response = []
                except exceptions.ClientRequestException as e:
                    self.log.error(
                        f"status_code:{e.status_code}, "
                        f"request_id:{e.request_id}, "
                        f"error_code:{e.error_code}. "
                        f"error_msg:{e.error_msg}")
            self.ecs_ips.append(self.ecs_ip)
            self.ecs_ip = []


    def get_eip_client(self, context):
        for region in self.region_ids:
            credentials = BasicCredentials(context.getAccessKey(), context.getSecretKey())
            client = eip.EipClient.new_builder() \
                .with_credentials(credentials) \
                .with_region(EipRegion.value_of(region)) \
                .build()
            self.list_eip_client.append(client)

    def create_eip(self):
        for index, client in enumerate(self.list_eip_client):
            try:
                request = eip.CreatePublicipRequest()
                publicipbody = eip.CreatePublicipOption(
                    type=self.eip_type
                )
                bandwidthbody = eip.CreatePublicipBandwidthOption(
                    name='global-accelerator-for-cross-border-e-commerce',
                    charge_mode="bandwidth",
                    share_type="PER",
                    size=5
                )
                request.body = eip.CreatePublicipRequestBody(
                    publicip=publicipbody,
                    bandwidth=bandwidthbody
                )
                response = client.create_publicip(request)
                self.eip_response.append(response.to_dict())
                self.eip_id.append(self.eip_response[0]['publicip']['id'])
                self.eip_ids.append(self.eip_id)
                self.eip_ip.append(self.eip_response[0]['publicip']['public_ip_address'])
                self.eip_ips.append(self.eip_ip)
                self.eip_response = []
                self.eip_id = []
                self.eip_ip = []
            except exceptions.ClientRequestException as e:
                self.log.error(
                               f"status_code:{e.status_code}, "
                               f"request_id:{e.request_id}, "
                               f"error_code:{e.error_code}. "
                               f"error_msg:{e.error_msg}")


    def create_eip_tag(self):
        for index, client in enumerate(self.list_eip_client):
            for item in self.eip_ids[index]:
                try:
                    request = eip.CreatePublicipTagRequest()
                    request.publicip_id = item
                    tagbody = eip.ResourceTagOption(
                        key="sac",
                        value="key"
                    )
                    request.body = eip.CreatePublicipTagRequestBody(
                        tag=tagbody
                    )
                    response = client.create_publicip_tag(request)
                except exceptions.ClientRequestException as e:
                    self.log.error(
                        f"status_code:{e.status_code}, "
                        f"request_id:{e.request_id}, "
                        f"error_code:{e.error_code}. "
                        f"error_msg:{e.error_msg}")

    def get_elb_client(self, context):
        for region in self.region_ids:
            credentials = BasicCredentials(context.getAccessKey(), context.getSecretKey())
            client = elb.ElbClient.new_builder() \
                .with_credentials(credentials) \
                .with_region(ElbRegion.value_of(region)) \
                .build()
            self.list_elb_client.append(client)

    def create_elb(self):
        for index, client in enumerate(self.list_elb_client):
            try:
                request = elb.CreateLoadBalancerRequest()
                listPublicipIdsLoadbalancer = self.eip_ids[index]
                listTagsLoadbalancer = [
                    elb.Tag(
                        key="sac",
                        value="key"
                    )
                ]
                listAvailabilityZoneListLoadbalancer = self.ecs_azs_ids[index]
                loadbalancerbody = elb.CreateLoadBalancerOption(
                    name='global-accelerator-for-cross-border-e-commerce',
                    vpc_id=self.vpc_ids[index],
                    availability_zone_list=listAvailabilityZoneListLoadbalancer,
                    tags=listTagsLoadbalancer,
                    publicip_ids=listPublicipIdsLoadbalancer
                )
                request.body = elb.CreateLoadBalancerRequestBody(
                    loadbalancer=loadbalancerbody
                )
                response = client.create_load_balancer(request)
                self.elb_response.append(response.to_dict())
                self.elb_ids.append(self.elb_response[0]['loadbalancer']['id'])
                self.elb_response = []
            except exceptions.ClientRequestException as e:
                self.log.error(
                               f"status_code:{e.status_code}, "
                               f"request_id:{e.request_id}, "
                               f"error_code:{e.error_code}. "
                               f"error_msg:{e.error_msg}")


    def create_elb_listener(self):
        for index, client in enumerate(self.list_elb_client):
            try:
                request = elb.CreateListenerRequest()
                listenerbody = elb.CreateListenerOption(
                    loadbalancer_id=self.elb_ids[index],
                    name='global-accelerator-for-cross-border-e-commerce',
                    protocol=self.elb_protocol,
                    protocol_port=self.elb_protocol_port
                )
                request.body = elb.CreateListenerRequestBody(
                    listener=listenerbody
                )
                response = client.create_listener(request)
            except exceptions.ClientRequestException as e:
                self.log.error(
                               f"status_code:{e.status_code}, "
                               f"request_id:{e.request_id}, "
                               f"error_code:{e.error_code}. "
                               f"error_msg:{e.error_msg}")

    def create_elb_pool(self):
        for index, client in enumerate(self.list_elb_client):
            try:
                request = elb.CreatePoolRequest()
                poolbody = elb.CreatePoolOption(
                    name='global-accelerator-for-cross-border-e-commerce',
                    protocol=self.elb_protocol,
                    lb_algorithm=self.elb_algorithm,
                    loadbalancer_id=self.elb_ids[index]
                )
                request.body = elb.CreatePoolRequestBody(
                    pool=poolbody
                )
                response = client.create_pool(request)
                self.elb_pool_response.append(response.to_dict())
                self.elb_pool_ids.append(self.elb_pool_response[0]["pool"]["id"])
                self.elb_pool_response = []
            except exceptions.ClientRequestException as e:
                self.log.error(
                               f"status_code:{e.status_code}, "
                               f"request_id:{e.request_id}, "
                               f"error_code:{e.error_code}. "
                               f"error_msg:{e.error_msg}")


    def create_elb_member(self):
        time.sleep(10)
        for index, client in enumerate(self.list_elb_client):
            for item in self.ecs_ips[index]:
                try:
                    request = elb.CreateMemberRequest()
                    request.pool_id = self.elb_pool_ids[index]
                    memberbody = elb.CreateMemberOption(
                        address=item,
                        protocol_port=self.elb_member_protocol_port,
                        subnet_cidr_id=self.ipv4_subnet_ids[index]
                    )
                    request.body = elb.CreateMemberRequestBody(
                        member=memberbody
                    )
                    response = client.create_member(request)
                except exceptions.ClientRequestException as e:
                    self.log.error(
                        f"status_code:{e.status_code}, "
                        f"request_id:{e.request_id}, "
                        f"error_code:{e.error_code}. "
                        f"error_msg:{e.error_msg}")

    def get_ga_client(self, context):
        credentials = BasicCredentials(context.getAccessKey(), context.getSecretKey())
        client = ga.GaClient.new_builder() \
            .with_credentials(credentials) \
            .with_region(GaRegion.value_of("cn-east-3")) \
            .build()
        return client

    def create_accelerator(self):
        try:
            request = ga.CreateAcceleratorRequest()
            listTagsAccelerator = [
                ga.ResourceTag(
                    key="sac",
                    value="key"
                )
            ]
            listIpSetsAccelerator = [
                ga.CreateAcceleratorOptionIpSets(
                    ip_type="IPV4",
                    area=self.ga_area
                )
            ]
            acceleratorbody = ga.CreateAcceleratorOption(
                name='global-accelerator-for-cross-border-e-commerce',
                ip_sets=listIpSetsAccelerator,
                enterprise_project_id="0",
                tags=listTagsAccelerator
            )
            request.body = ga.CreateAcceleratorRequestBody(
                accelerator=acceleratorbody
            )
            response = self.ga_client.create_accelerator(request)
            self.list_accelerators.append(response.to_dict())
        except exceptions.ClientRequestException as e:
            self.log.error(
                f"status_code:{e.status_code}, "
                f"request_id:{e.request_id}, "
                f"error_code:{e.error_code}. "
                f"error_msg:{e.error_msg}")

    def create_listener(self):
        for item in self.ga_port_list:
            try:
                request = ga.CreateListenerRequest()
                listTagsListener = [
                    ga.ResourceTag(
                        key="sac",
                        value="key"
                    )
                ]
                listPortRangesListener = [ga.PortRange(from_port=item[0], to_port=item[1])]
                listenerbody = ga.CreateListenerOption(
                    name='global-accelerator-for-cross-border-e-commerce',
                    protocol=self.ga_protocol,
                    port_ranges=listPortRangesListener,
                    accelerator_id=self.list_accelerators[0]['accelerator']['id'],
                    tags=listTagsListener
                )
                request.body = ga.CreateListenerRequestBody(
                    listener=listenerbody
                )
                response = self.ga_client.create_listener(request)
                self.list_listeners.append(response.to_dict())
            except exceptions.ClientRequestException as e:
                self.log.error(
                               f"status_code:{e.status_code}, "
                               f"request_id:{e.request_id}, "
                               f"error_code:{e.error_code}. "
                               f"error_msg:{e.error_msg}")

    def create_endpoint_group(self):
        time.sleep(20)
        for index, item in enumerate(self.list_listeners):
            try:
                request = ga.CreateEndpointGroupRequest()
                listListenersEndpointGroup = [ga.Id(id=item['listener']['id'])]
                for i, region_id in enumerate(self.region_ids):
                    endpointGroupbody = ga.CreateEndpointGroupOption(
                        name=self.ga_endpoint_group[i],
                        region_id=region_id,
                        listeners=listListenersEndpointGroup
                    )
                    request.body = ga.CreateEndpointGroupRequestBody(
                        endpoint_group=endpointGroupbody
                    )
                    response = self.ga_client.create_endpoint_group(request)
                    self.list_endpoint_groups.append(response.to_dict())
            except exceptions.ClientRequestException as e:
                self.log.error(
                               f"status_code:{e.status_code}, "
                               f"request_id:{e.request_id}, "
                               f"error_code:{e.error_code}. "
                               f"error_msg:{e.error_msg}")

    def create_healthcheck(self):
        time.sleep(20)
        for index, item in enumerate(self.list_endpoint_groups):
            try:
                request = ga.CreateHealthCheckRequest()
                healthCheckbody = ga.CreateHealthCheckOption(
                    endpoint_group_id=item['endpoint_group']['id'],
                    protocol="TCP",
                    port=self.ga_healthcheck_port,
                    interval=30,
                    timeout=30,
                    max_retries=2,
                    enabled=True
                )
                request.body = ga.CreateHealthCheckRequestBody(
                    health_check=healthCheckbody
                )
                response = self.ga_client.create_health_check(request)
            except exceptions.ClientRequestException as e:
                self.log.error(
                               f"status_code:{e.status_code}, "
                               f"request_id:{e.request_id}, "
                               f"error_code:{e.error_code}. "
                               f"error_msg:{e.error_msg}")

    def create_endpoint(self):
        time.sleep(20)
        for index, item in enumerate(self.list_endpoint_groups):
            try:
                request = ga.CreateEndpointRequest()
                request.endpoint_group_id = item['endpoint_group']['id']
                for i, resource_id in enumerate(self.eip_ids[index]):
                    endpointbody = ga.CreateEndpointOption(
                        resource_id=resource_id,
                        resource_type="EIP",
                        ip_address=self.eip_ips[index][i]
                    )
                    request.body = ga.CreateEndpointRequestBody(
                        endpoint=endpointbody
                    )
                    response = self.ga_client.create_endpoint(request)
            except exceptions.ClientRequestException as e:
                self.log.error(
                               f"status_code:{e.status_code}, "
                               f"request_id:{e.request_id}, "
                               f"error_code:{e.error_code}. "
                               f"error_msg:{e.error_msg}")

    def start_process(self):
        self.create_vpc()
        self.create_vpc_tag()
        self.create_subnet()
        self.create_subnet_tag()
        self.list_ecs_az()
        self.create_ecs()
        self.show_ecs()
        self.create_eip()
        self.create_eip_tag()
        self.create_elb()
        self.create_elb_listener()
        self.create_elb_pool()
        self.create_elb_member()
        self.create_accelerator()
        self.create_listener()
        self.create_endpoint_group()
        self.create_healthcheck()
        self.create_endpoint()
