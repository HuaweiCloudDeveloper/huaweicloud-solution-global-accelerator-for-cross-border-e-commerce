[TOC]

**解决方案介绍**
===============
该方案帮助您一键部署全球加速服务GA，为全球化业务应用加速。GA服务会为相应加速区域分配Anycast IP，客户端流量将通过Anycast IP接入华为全球骨干网络，可以帮助用户提供高可靠、低时延、易管理、安全合规的网络环境，使您在全球范围内的用户能够快速访问后端应用。

解决方案实践详情页面地址：https://www.huaweicloud.com/solution/implementations/global-accelerator-for-cross-border-e-commerce.html

**架构图**
---------------
![方案架构](./document/global-accelerator-for-cross-border-e-commerce.png)

**架构描述**
---------------
该解决方案会部署如下资源：

- 创建一个全球加速服务GA，伪分布在全球的业务提供加速服务


**组织结构**
---------------

``` lua
huaweicloud-solution-global-accelerator-for-cross-border-e-commerce
├── global-accelerator-for-cross-border-e-commerce.tf.json -- 资源编排模板
├── global-accelerator-for-cross-border-e-commerce-enhanced.tf.json -- 资源编排模板
├── functiongraph
	├── global-accelerator-for-cross-border-e-commerce-enhanced-create.py  -- 创建资源函数文件
	├── global-accelerator-for-cross-border-e-commerce-enhanced-delete.py  -- 删除资源函数文件
```
**开始使用**
---------------

1.登录[GA控制台](https://console.huaweicloud.com/ga/?locale=zh-cn&region=cn-north-4#/accelerators/list)，查看已经部署好的GA加速实例

图1 加速实例

![加速实例](./document/readme-image-001.png)

2.选择“监听器”，即可查看监听器监听终端节点组的端口。

图2 监听器

![监听器](./document/readme-image-002.png)

3.选择“终端节点组”，即可查看已创建好的终端节点组和终端节点。

图3 终端节点组

![终端节点组](./document/readme-image-003.png)



